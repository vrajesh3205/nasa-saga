import axios from "axios";

export const asteroidService = (id) => {
  return axios.get(
    `${process.env.REACT_APP_API_FIXED}${id}?${process.env.REACT_APP_API_KEY}`
  );
};

import { Box, Typography } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
const NasaUserDetail = () => {
  const detail = useSelector((state) => {
    return state.data;
  });
  return (
    <div>
      <Box>
        <Typography>
          <b>Name: </b>
          {detail.name}
        </Typography>
        <Typography>
          <b>Nasa Jpl Url: </b>
          {detail.nasa_jpl_url}
        </Typography>
        <Typography>
          <b>Is Potentially Hazardous Asteroid</b>
          {detail.is_potentially_hazardous_asteroid ? "true" : "false"}
        </Typography>
      </Box>
    </div>
  );
};
export default NasaUserDetail;

import { Box, Button, TextField } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { userAction } from "../Actions/userAction";
const NasaUserForm = () => {
  const [id, setId] = useState(null);
  const dispatch = useDispatch();
  const handleId = () => {
    dispatch(userAction(id));
  };
  const changeName = (e) => {
    setId(e.target.value);
  };
  return (
    <div>
      <Box>
        <h1>Nasa User Form</h1>
        <TextField
          name={id}
          variant="outlined"
          color="primary"
          label="User Id"
          onChange={changeName}
        />
        <Link to={{ pathname: "/NasaUserDetail" }}>
          <Button
            onClick={handleId}
            type="submit"
            variant="outlined"
            color="primary"
          >
            Submit
          </Button>
        </Link>
      </Box>
    </div>
  );
};
export default NasaUserForm;

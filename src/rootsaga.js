import { all } from "redux-saga/effects";
import { waitForFetchAsteroid } from "./Sagas/asteroidSaga";

export default function* rootsaga() {
  yield all([waitForFetchAsteroid()]);
}

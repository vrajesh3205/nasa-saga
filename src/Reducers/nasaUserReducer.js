import * as actionTypes from "../Type/actionType";
const initialState = {
  data: [],
};
export const nasaUserReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionTypes.setasteroidAction:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};

import { takeEvery, call, put } from "redux-saga/effects";
import * as actions from "../Actions/userAction";
import { asteroidService } from "../Services/asteroidService";
import * as actionType from "../Type/actionType";
function* getasteroidInfo({ id }) {
  console.log("ok");
  try {
    const details = yield call(asteroidService, id);
    yield put(actions.setUserAction(details));
  } catch (e) {
    console.log(e);
  }
}
export function* waitForFetchAsteroid() {
  yield takeEvery(actionType.asteroidAction, getasteroidInfo);
}

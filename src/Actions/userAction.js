import * as actionTypes from "../Type/actionType";
export const userAction = (id) => {
  return { type: actionTypes.asteroidAction, id };
};
export const setUserAction = (details) => {
  if (details) {
    return {
      type: actionTypes.setasteroidAction,
      payload: details.data,
    };
  }
};
